import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'sel-sign-up',
  templateUrl:'sign-up.component.html'
})

export class SignUpPage {
  constructor(public navCtrl: NavController) {}

}
