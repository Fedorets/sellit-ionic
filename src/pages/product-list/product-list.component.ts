import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'sel-product-list',
  templateUrl:'product-list.component.html'
})

export class ProductListPage {
  constructor(public navCtrl: NavController) {}
}
