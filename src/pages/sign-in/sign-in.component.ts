import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'sel-sign-in',
  templateUrl:'sign-in.component.html'
})

export class SignInPage {
  constructor(public navCtrl: NavController) {}

}
