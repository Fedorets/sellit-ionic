import { User } from './user.model';
import { Photo } from './photo.model';

export class Product  {

    public id: number;
    public title: string;
    public description: string;
    public price: number;
    public dateCreate: string;
    public dateUpdate: string;
    public userDetails: User;
    private _photos = [];

    constructor(data) {
          this.id = data.id;
          this.title = data.title;
          this.description = data.description;
          this.price = data.price;
          this.dateCreate = data.date_create;
          this.dateUpdate = data.date_update;
          this.userDetails = new User(data.author_details);
          data.photo_details.forEach((index) => {
              this._photos.push(new Photo(index));
          });
      }

    get photos() {
        return this._photos;
    }

    set photos(photos){
        this._photos.push(new Photo(photos));
    }
}
