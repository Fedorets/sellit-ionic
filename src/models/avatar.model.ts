import { ConfigService } from '../providers/config.service';

export class Avatar {
    public id: number;
    private _photo: string;
    constructor(data) {
        this.id = data.id;
        this._photo = data.photo;
    }
    get photo() {
        if (this._photo !== null) {
            return ConfigService.imagePath + this._photo;
        } else { return null; }
    }
}
